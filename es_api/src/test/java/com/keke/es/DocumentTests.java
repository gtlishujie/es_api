package com.keke.es;

import com.alibaba.fastjson.JSON;
import com.keke.es.pojo.User;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName: DocumentTests
 * @Description: es7.6.x 高级客户端测试 文档API
 * @Author: keke
 * @Date: 2021/9/10
 */
public class DocumentTests extends EsApiApplicationTests {

    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;

    /**
     * @Author keke
     * @Description 添加文档
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    @Test
    void testAddDocument() throws IOException {
        //创建对象
        User user = new User("keke", 18);
        //创建请求
        IndexRequest request = new IndexRequest("keke_index");

        //规则 put/keke_index/_doc/1
        //设置请求过期时间
        request.id("1");
//        request.type("_doc"); //es7.6.1已经不用指定类型
        request.timeout(TimeValue.timeValueSeconds(1));
//        request.timeout("1s"); //同上

        //将数据放入请求 json
        request.source(JSON.toJSONString(user), XContentType.JSON);

        //客户端发送请求
        IndexResponse response = client.index(request, RequestOptions.DEFAULT);

        System.out.println(response.toString());
        System.out.println(response.status()); //对应命令的返回状态 CREATED
    }

    /**
     * @Author keke
     * @Description 判断文档是否存在
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    @Test
    void testExistDocument() throws IOException {
        GetRequest request = new GetRequest("keke_index", "1");
        //不获取返回的_source的上下文了
        request.fetchSourceContext(new FetchSourceContext(false));
        request.storedFields("_none_");

        boolean exists = client.exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);
    }

    /**
     * @Author keke
     * @Description 获取文档的信息
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    @Test
    void testGetDocument() throws IOException {
        GetRequest request = new GetRequest("keke_index", "1");
        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        System.out.println(response); //这里返回的全部内容和命令是一样的
        System.out.println(response.getSourceAsString()); //打印文档的内容
    }

    /**
     * @Author keke
     * @Description 更新文档的信息
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    @Test
    void testUpdateDocument() throws IOException {
        UpdateRequest request = new UpdateRequest("keke_index", "1");
        request.timeout("1s");

        User user = new User("keke不会Java", 18);
        request.doc(JSON.toJSONString(user), XContentType.JSON);

        UpdateResponse response = client.update(request, RequestOptions.DEFAULT);
        System.out.println(response.status());
    }

    /**
     * @Author keke
     * @Description 删除文档的记录
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    @Test
    void testDeleteDocument() throws IOException {
        DeleteRequest request = new DeleteRequest("keke_index", "1");
        request.timeout("1s");

        DeleteResponse response = client.delete(request, RequestOptions.DEFAULT);
        System.out.println(response.status());
    }

    /**
     * @Author keke
     * @Description 批量添加文档
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    @Test
    void testBulkAddDocument() throws IOException {
        BulkRequest request = new BulkRequest();
        request.timeout("10s");

        List<User> userList = new ArrayList<>();
        userList.add(new User("keke1", 1));
        userList.add(new User("keke2", 2));
        userList.add(new User("keke3", 3));
        userList.add(new User("keke4", 4));
        userList.add(new User("keke5", 5));
        userList.add(new User("keke6", 6));

        //批处理请求
        for (int i = 0; i < userList.size(); i++) {
            //批量更新或删除，在这里修改对应的请求即可
            request.add(new IndexRequest("keke_index")
//                            .id("" + (i+1)) //不设置id，会自动生成一个不重复的随机id
                            .source(JSON.toJSONString(userList.get(i)), XContentType.JSON));
        }

        BulkResponse response = client.bulk(request, RequestOptions.DEFAULT);
        System.out.println(response.hasFailures()); //是否失败，返回false，代表成功
    }

    /**
     * @Author keke
     * @Description 查询文档
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    //SearchRequest 搜索请求
    //SearchSourceBuilder 条件构造
    //HighlightBuilder 高亮构造
    //TermQueryBuilder 精确查询
    //MatchAllQueryBuilder 匹配全部
    //XxxQueryBuilder 对应es请求命令
    @Test
    void testSearchDocument() throws IOException {
        SearchRequest request = new SearchRequest("keke_index");
        //构建搜索条件
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //查询条件，可以使用QueryBuilders工具类来构建
        //QueryBuilders.termQuery 精确匹配
        //QueryBuilders.matchAllQuery 匹配所有
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "keke1");
//        MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
        sourceBuilder.query(termQueryBuilder);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        request.source(sourceBuilder);

        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        System.out.println(JSON.toJSONString(response.getHits()));
        System.out.println("=======================================");
        for (SearchHit hit : response.getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }

}
