package com.keke.es;

import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;

/**
 * @ClassName: IndexTests
 * @Description: es7.6.x 高级客户端测试 索引API
 * @Author: keke
 * @Date: 2021/9/10
 */
public class IndexTests extends EsApiApplicationTests {

    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;

    /**
     * @Author keke
     * @Description 创建索引
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    @Test
    void testCreateIndex() throws IOException {
        //1、创建索引请求
        CreateIndexRequest request = new CreateIndexRequest("keke_index");
        //2、客户端执行请求，IndicesClient，请求后获得响应
        CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
        System.out.println(createIndexResponse);
    }

    /**
     * @Author keke
     * @Description 判断索引是否存在
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    @Test
    void testExistIndex() throws IOException {
        GetIndexRequest request = new GetIndexRequest("keke_index");
        boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);
    }

    /**
     * @Author keke
     * @Description 删除索引
     * @Date 2021/9/10
     * @Param []
     * @Return void
     **/
    @Test
    void testDeleteIndex() throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest("keke_index");
        AcknowledgedResponse delete = client.indices().delete(request, RequestOptions.DEFAULT);
        System.out.println(delete.isAcknowledged());
    }

}
