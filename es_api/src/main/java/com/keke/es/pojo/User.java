package com.keke.es.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @ClassName: User
 * @Description:
 * @Author: keke
 * @Date: 2021/9/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class User {

    private String name;

    private Integer age;

}
