package com.keke.es.controller;

import com.keke.es.service.ContentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: ContentController
 * @Description:
 * @Author: keke
 * @Date: 2021/9/11
 */
@RestController
public class ContentController {

    @Resource
    private ContentService contentService;

    /**
     * @Author keke
     * @Description 解析数据存放到es索引中
     * @Date 2021/9/11
     * @Param [keyword]
     * @Return java.lang.Boolean
     **/
    @GetMapping("/parseContent/{keyword}")
    public Boolean parseContent(@PathVariable("keyword") String keyword) throws Exception {
        return contentService.parseContent(keyword);
    }

    /**
     * @Author keke
     * @Description 搜索数据
     * @Date 2021/9/11
     * @Param [keywords, page, pageSize]
     * @Return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    @GetMapping("/searchPageContent/{keyword}/{page}/{pageSize}")
    public List<Map<String, Object>> searchPageContent(@PathVariable("keyword") String keywords,
                                                       @PathVariable("page") Integer page,
                                                       @PathVariable("pageSize") Integer pageSize) throws Exception {
        return contentService.searchPageContent(keywords, page, pageSize);
    }

    /**
     * @Author keke
     * @Description 搜索数据高亮显示
     * @Date 2021/9/11
     * @Param [keywords, page, pageSize]
     * @Return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    @GetMapping("/searchPageContentHighlighter/{keyword}/{page}/{pageSize}")
    public List<Map<String, Object>> searchPageContentHighlighter(@PathVariable("keyword") String keywords,
                                                       @PathVariable("page") Integer page,
                                                       @PathVariable("pageSize") Integer pageSize) throws Exception {
        return contentService.searchPageContentHighlighter(keywords, page, pageSize);
    }

}
