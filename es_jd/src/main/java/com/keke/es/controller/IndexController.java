package com.keke.es.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @ClassName: IndexController
 * @Description:
 * @Author: keke
 * @Date: 2021/9/10
 */
@Controller
public class IndexController {

    /**
     * @Author keke
     * @Description 访问项目首页
     * @Date 2021/9/10
     * @Param []
     * @Return java.lang.String
     **/
    @GetMapping({"/", "/index"})
    public String index() {
        return "/index";
    }

}
