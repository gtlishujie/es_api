package com.keke.es.service.impl;

import com.alibaba.fastjson.JSON;
import com.keke.es.pojo.Content;
import com.keke.es.service.ContentService;
import com.keke.es.util.HtmlParseUtils;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName: ContentServiceImpl
 * @Description:
 * @Author: keke
 * @Date: 2021/9/11
 */
@Service
public class ContentServiceImpl implements ContentService {

    @Resource
    private RestHighLevelClient restHighLevelClient;

    //restHighLevelClient在spring容器中，故报错NullPointerException
//    public static void main(String[] args) throws Exception {
//        new ContentServiceImpl().parseContent("java");
//    }

    @Override
    public Boolean parseContent(String keywords) throws Exception {
        //判断索引是否存在
        GetIndexRequest request = new GetIndexRequest("jd_goods");
        boolean exists = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
        if (!exists) {
            //创建索引
            CreateIndexRequest createIndexRequest = new CreateIndexRequest("jd_goods");
            restHighLevelClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
        }

        List<Content> contentList = HtmlParseUtils.parseJD(keywords);
        //把查询到的数据放入es
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout("2m");

        for (Content content : contentList) {
            bulkRequest.add(new IndexRequest("jd_goods")
                    .source(JSON.toJSONString(content), XContentType.JSON));
        }

        BulkResponse response = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        return !response.hasFailures();
    }

    @Override
    public List<Map<String, Object>> searchPageContent(String keywords, Integer page, Integer pageSize) throws Exception {

        if (page == null || page <= 0) {
            page = 1;
        }
        if (pageSize == null || pageSize <= 0) {
            pageSize = 10;
        }

        SearchRequest searchRequest = new SearchRequest("jd_goods");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //分页
        int start = (page - 1) * pageSize;
        sourceBuilder.from(start);
        sourceBuilder.size(pageSize);

        //精确匹配
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", keywords);
        sourceBuilder.query(termQueryBuilder);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        //执行搜索
        searchRequest.source(sourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        List<Map<String, Object>> mapList = new LinkedList<>();

        //结果解析
        SearchHit[] hits = searchResponse.getHits().getHits();
        for (SearchHit hit : hits) {
            mapList.add(hit.getSourceAsMap());
        }

        return mapList;
    }

    @Override
    public List<Map<String, Object>> searchPageContentHighlighter(String keywords, Integer page, Integer pageSize) throws Exception {

        if (page == null || page <= 0) {
            page = 1;
        }
        if (pageSize == null || pageSize <= 0) {
            pageSize = 10;
        }

        SearchRequest searchRequest = new SearchRequest("jd_goods");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //分页
        int start = (page - 1) * pageSize;
        sourceBuilder.from(start);
        sourceBuilder.size(pageSize);

        //精确匹配
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", keywords);
        sourceBuilder.query(termQueryBuilder);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        //高亮
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        highlightBuilder.field("title");
        //关闭title中多个搜索符合字段高亮
//        highlightBuilder.requireFieldMatch(false);
        highlightBuilder.preTags("<span style='color:red'>");
        highlightBuilder.postTags("</span>");
        sourceBuilder.highlighter(highlightBuilder);

        //执行搜索
        searchRequest.source(sourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        List<Map<String, Object>> mapList = new LinkedList<>();

        //结果解析
        SearchHit[] hits = searchResponse.getHits().getHits();
        for (SearchHit hit : hits) {
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField title = highlightFields.get("title");
            Map<String, Object> sourceAsMap = hit.getSourceAsMap(); //原来的结果！
            //解析高亮字段，将原来的字段替换成高亮的字段
            if (title != null) {
                Text[] fragments = title.getFragments();
                String newTitle = "";
                for (Text text : fragments) {
                    newTitle += text;
                }
                sourceAsMap.put("title", newTitle); //高亮字段替换掉原来的内容
            }
            mapList.add(sourceAsMap);
        }

        return mapList;
    }

}
