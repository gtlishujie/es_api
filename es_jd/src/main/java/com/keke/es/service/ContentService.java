package com.keke.es.service;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: ContentService
 * @Description:
 * @Author: keke
 * @Date: 2021/9/11
 */
public interface ContentService {

    /**
     * @Author keke
     * @Description 解析数据存放到es索引中
     * @Date 2021/9/11
     * @Param [keywords]
     * @Return java.lang.Boolean
     **/
    Boolean parseContent(String keywords) throws Exception;

    /**
     * @Author keke
     * @Description 搜索数据
     * @Date 2021/9/11
     * @Param [keywords, page, pageSize]
     * @Return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<Map<String, Object>> searchPageContent(String keywords, Integer page, Integer pageSize) throws Exception;

    /**
     * @Author keke
     * @Description 搜索数据高亮显示
     * @Date 2021/9/11
     * @Param [keywords, page, pageSize]
     * @Return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     **/
    List<Map<String, Object>> searchPageContentHighlighter(String keywords, Integer page, Integer pageSize) throws Exception;

}
