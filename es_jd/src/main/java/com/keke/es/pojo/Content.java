package com.keke.es.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: content
 * @Description:
 * @Author: keke
 * @Date: 2021/9/11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Content {

    private String title;

    private String img;

    private String price;

}
