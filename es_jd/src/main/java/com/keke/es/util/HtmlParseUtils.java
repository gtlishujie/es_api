package com.keke.es.util;

import com.keke.es.pojo.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: HtmlParseUtils
 * @Description:
 * @Author: keke
 * @Date: 2021/9/11
 */
public class HtmlParseUtils {

    public static void main(String[] args) throws Exception {
       parseJD("java").forEach(System.out::println);
    }

    public static List<Content> parseJD(String keyword) throws Exception {
        //获取请求 https://search.jd.com/Search?keyword=java
        //前提：需要联网
        String url = "https://search.jd.com/Search?keyword=" + keyword;
        //解析网页（jsoup返回Document就是浏览器的Document对象）
        //keyword为中文的话，这里的url要设置编码
        Document document = Jsoup.parse(new URL(url), 30000);
        //所有在js中可以使用的方法，在这里都能用
        Element element = document.getElementById("J_goodsList");
//        System.out.println(element.html());
        //获取所有的li元素
        Elements elements = element.getElementsByTag("li");

        List<Content> goodsList = new ArrayList<>();

        //获取元素中的内容，这里的el就是每一个li标签
        for (Element el : elements) {
            //关于这种图片特别多的网站，所有的图片都是延迟加载的
            //京东图片信息放在这里source-data-lazy-img
//            String img = el.getElementsByTag("img").eq(0).attr("src"); //null
            String img = el.getElementsByTag("img").eq(0).attr("data-lazy-img");
            String price = el.getElementsByClass("p-price").eq(0).text();
            String title = el.getElementsByClass("p-name").eq(0).text();

            Content content = new Content();
            content.setTitle(title);
            content.setImg(img);
            content.setPrice(price);

            goodsList.add(content);
        }

        return goodsList;
    }

}
